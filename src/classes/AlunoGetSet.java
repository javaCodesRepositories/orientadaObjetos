package classes;

public class AlunoGetSet {

//	ATRIBUTOS do Aluno
	private String nome;
	private int idade;
	private String dataNascimento;
	private String cpf;
	private String rg;
	private String nomeMae;
	private String nomePai;
	private String dataMatricula;
	private String matricula;
	private String curso;
	private String serieMatriculado;
	private String nomeEscola;
	
	/* CONSTRUTOR - Cria os dados na memoria - Sendo padrao do Java */
	public AlunoGetSet() { 
		// TODO Auto-generated constructor stub
	}
	
//	CONSTRUTOR ja passando o nome do aluno como padrao ao criar o Objeto
	public AlunoGetSet(String nomePadrao) {
		nome = nomePadrao;
	}
	
//	CONSTRUTOR ja passando o nome e idade do aluno como padrao ao criar o Objeto
	public AlunoGetSet(String nomePadrao, int idadePadrao) {
		nome = nomePadrao;
		idade = idadePadrao;
	}
	
	/* Veremos os metodos GETTERS e SETTERS do Objeto 
	 * SET eh para setar valores dos atributos
	 * GET eh para buscar os valores do atributos */
	
	/* Metodo SET */
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public String getNomePai() {
		return nomePai;
	}

	public void setNomePai(String nomePai) {
		this.nomePai = nomePai;
	}

	public String getDataMatricula() {
		return dataMatricula;
	}

	public void setDataMatricula(String dataMatricula) {
		this.dataMatricula = dataMatricula;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}

	public String getSerieMatriculado() {
		return serieMatriculado;
	}

	public void setSerieMatriculado(String serieMatriculado) {
		this.serieMatriculado = serieMatriculado;
	}

	public String getNomeEscola() {
		return nomeEscola;
	}

	public void setNomeEscola(String nomeEscola) {
		this.nomeEscola = nomeEscola;
	}
	
}
