package classes;

public class AlunoPublicPrivate {

//	ATRIBUTOS do Aluno
	public String nome = "";
	public int Idade = 0;
	private String dataNascimento = "";
	String cpf = "";
	String rg = "";
	String nomeMae = "";
	String nomePai = "";
	String dataMatricula = "";
	String matricula = "";
	String curso = "";
	String serieMatriculado = "";
	String nomeEscola = "";
	
	/* CONSTRUTOR - Cria os dados na memoria - Sendo padrao do Java */
	public AlunoPublicPrivate() { 
		// TODO Auto-generated constructor stub
	}
	
//	CONSTRUTOR ja passando o nome do aluno como padrao ao criar o Objeto
	public AlunoPublicPrivate(String nomePadrao) {
		nome = nomePadrao;
	}
	
//	CONSTRUTOR ja passando o nome e idade do aluno como padrao ao criar o Objeto
	public AlunoPublicPrivate(String nomePadrao, int idadePadrao) {
		nome = nomePadrao;
		Idade = idadePadrao;
	}
	
}
