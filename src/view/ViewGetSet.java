package view;

import classes.AlunoGetSet;

public class ViewGetSet {

	public static void main(String[] args) {
	
//		Agora sim, aluno esta instanciado e existe na memoria
//		VARIAVEL aluno eh uma REFERENCIA para o Objeto Aluno
		AlunoGetSet aluno = new AlunoGetSet();
		
		aluno.setNome("Gustavo");
		aluno.setIdade(40);
		aluno.setDataNascimento("18/10/1987");
		aluno.setCpf("107.775.376-49");
		aluno.setRg("MG-070.879");
		aluno.setNomeMae("Raquel Lucas");
		aluno.setNomePai("Joaquim Ramos");
		aluno.setDataMatricula("08/12/2020");
		aluno.setMatricula("71101462");
		aluno.setCurso("Sistema de Informacao");
		aluno.setSerieMatriculado("8");
		aluno.setNomeEscola("Faculdade Cotemig");
		
		
		System.out.println("Nome eh: " + aluno.getNome());
		System.out.println("Idade eh: " + aluno.getIdade());
		System.out.println("Data de Nascimento eh: " + aluno.getDataNascimento());
		System.out.println("CPF eh :" + aluno.getCpf());
		System.out.println("RG eh: " + aluno.getRg());
		System.out.println("Nome da Mae: " + aluno.getNomeMae());
		System.out.println("Nome do Pai: " + aluno.getNomePai());
		System.out.println("Data da Matricula: " + aluno.getDataMatricula());
		System.out.println("Numero da Matricula: " + aluno.getMatricula());
		System.out.println("Nome do Curso eh: " + aluno.getCurso());
		System.out.println("Periodo Matriculado: " + aluno.getSerieMatriculado() + " periodo.");
		System.out.println("Nome da Faculdade: " + aluno.getNomeEscola());
		
		
		/*-------------------------------------------------------------*/
		AlunoGetSet aluno2 = new AlunoGetSet();
		aluno2.setNome("Roberio");
		aluno2.setIdade(29);
		aluno2.setDataNascimento("24/09/1989");
		
		System.out.println("\nNome : " + aluno2.getNome());
		System.out.println("Idade : " + aluno2.getIdade());
		System.out.println("Data de Nascimento: " + aluno2.getDataNascimento());
		
		
		
	}
}
