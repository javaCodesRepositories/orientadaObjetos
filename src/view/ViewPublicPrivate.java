package view;

import classes.AlunoPublicPrivate;

public class ViewPublicPrivate {

	public static void main(String[] args) {
		
//		Objeto ainda NAO INSTANCIADO e nao existe na memoria
		AlunoPublicPrivate aluno1;
		
//		Agora sim, aluno esta instanciado e existe na memoria
//		VARIAVEL aluno eh uma REFERENCIA para o Objeto Aluno
		AlunoPublicPrivate aluno = new AlunoPublicPrivate();
		
		AlunoPublicPrivate aluno2 = new AlunoPublicPrivate("Joao");
		
		AlunoPublicPrivate aluno3 = new AlunoPublicPrivate("Augusto", 28);
		
		
		
		
		aluno.nome = "Rodolffo";
		aluno.Idade = 40;
	
		System.out.println("Nome do Aluno eh: " + aluno.nome + "\nIdade eh: " + aluno.Idade);
	}
}
