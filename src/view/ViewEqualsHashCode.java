package view;

import classes.Aluno;

public class ViewEqualsHashCode {

	public static void main(String[] args) {
		
		/* EQUALS e HASHCODE (Diferenciar e Comparar Objetos) */
		Aluno aluno1 = new Aluno();
		aluno1.setNome("Gustavo");
		aluno1.setCpf("123");
		
		Aluno aluno2 = new Aluno();
		aluno2.setNome("Gustavo");
		aluno2.setCpf("123");
		
		System.out.println("HashCode Aluno 1: " + aluno1.hashCode());
		System.out.println("HashCode Aluno 2: " + aluno2.hashCode());
		
		
		// PODE SE USAR COMPARACAO HASHCODE()
		if(aluno1.hashCode() == aluno2.hashCode()) {
			System.out.println("\nAlunos sao iguais");
		}else {
			System.out.println("\nAlunos sao diferentes");
		}
		
		// OU USAR COMPARACAO EQUALS, da no mesmo
		if(aluno1.equals(aluno2)) {
			System.out.println("\nAlunos sao iguais");
		}else {
			System.out.println("\nAlunos sao diferentes");
		}
		
	}
}
